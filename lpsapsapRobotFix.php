
<?php
set_time_limit(600);
include 'arrayToCsv.php';

class LPSfixer 
{

	private $dir = 'C:/ScrapeDataHistory/';
	private $historyDIRs;
	
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	public function getAllHistoryDIR()
	{
        $this->historyDIRs = scandir($this->dir);
            if(!empty($this->historyDIRs)):
            /*
             * parse the csv data into sql data
             */
			 echo '<pre>';
            foreach($this->historyDIRs as $theDIR){
                if(isset($theDIR) && !empty($theDIR) && ($theDIR != ".." && $theDIR != ".")){
                    $this->parseData($theDIR);
					echo '<br>';
				}
				
			}
			
        endif;
	}
	
	private function parseData($historyDir)
	{
		if(is_dir( $this->dir.$historyDir."/lps/")){
			$lpsDIR = $this->dir.$historyDir."/lps/";
			$CountyDirs =scandir($lpsDIR);
			if(!empty($CountyDirs)){
				foreach($CountyDirs as $CountyDir){
					if($CountyDir != ".." && $CountyDir != "."){
						$files = glob($lpsDIR.$CountyDir."/*.csv");
						foreach($files as $file){
						
							$data = $this->convertToArray($file);
							
							
							$counter = 0;
							foreach($data as $item){ //this will add new column called 'Date-Time Scraped'
								
								if($counter == 0){
									$data[$counter][16] = 'Date-Time Scraped';
								}else{
									list($month, $day, $year) = explode("-", trim($historyDir));
									$data[$counter][16] = $year."-".$month."-".$day." 00:00:00";
								} 
								
								$counter++;
							}//end foreach 
						
							$csv = array_to_scv($data, false);
							file_put_contents($file, $csv);
						}
					}
				} //end foreach
			} //end if 
		}
		 
	}
	
	function fixCSVs()
	{
		$historyDir = 'C:/ScrapeData';
		if(is_dir($historyDir."/lps/")){
			$lpsDIR =$historyDir."/lps/";
			$CountyDirs =scandir($lpsDIR);
			if(!empty($CountyDirs)){
				foreach($CountyDirs as $CountyDir){
					if($CountyDir != ".." && $CountyDir != "."){
						$files = glob($lpsDIR.$CountyDir."/*.csv");
						foreach($files as $file){
						
							$data = $this->convertToArray($file);
							
							
							$counter = 0;
							foreach($data as $item){ //this will add new column called 'Date-Time Scraped'
								
								if($counter == 0){
									$data[$counter][16] = 'Date-Time Scraped';
								}else{
									
									$data[$counter][16] = date("Y-m-d H:i:s");
								} 
								
								$counter++;
							}//end foreach 
						
							$csv = array_to_scv($data, false);
							file_put_contents($file, $csv);
						}
					}
				} //end foreach
			} //end if 
		}
		 
	} //end of function
	
	function fatrusteeCSVfix(){
		$historyDir = 'C:/ScrapeData';
		if(is_dir($historyDir."/fatrustee/")){
			$CountyDirs =scandir($historyDir."/fatrustee/");
			
			foreach($CountyDirs as $dir){
				if($dir != ".." && $dir != "."){
					$files = glob($historyDir."/fatrustee/".$dir."/*.csv");
					foreach($files as $file){
						$data = $this->convertToArray($file);
						echo '<pre>';
						print_r($data);
					}
					
				}
			}
			
		}
	}//end of function
	

}

$rparser = new LPSfixer();
$rparser->fatrusteeCSVfix();



?>