<?php 
include 'arrayToCsv.php';
$currentHistoryDIR = "";
function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
}
function fixCSVHEAD($arraydata, $newFormat, $count){
	foreach($arraydata as $item){
		#if($item != ""){
			$newFormat[$count][]= $item;
		#}
	}
	return $newFormat;
}


function fixheader($files, $currentHistoryDIR = ''){
	$header = array("Trustee Sale No", "Property Address", "Sale date", "Sale Info", "Status", "Date-Time Scraped");
	foreach($files as $csvLocation){

		if(file_exists($csvLocation)){
			$data = convertToArray($csvLocation);
			
			array_shift($data); 
			array_unshift($data,$header);
			echo '<pre>';
			$arrayCount = count($data);
			if(isset($data[$arrayCount-1]) && isset($data[$arrayCount-2])){
				if(is_numeric($data[$arrayCount-2][0]) && is_numeric($data[$arrayCount-1][0])){
					unset($data[$arrayCount-2]);
					unset($data[$arrayCount-1]);
				}
					
			}
			
			$c = 0;
			foreach($data as $item){
				if($c != 0){
					if($currentHistoryDIR != ''){
						list($month, $day, $year) = explode("-", trim($currentHistoryDIR));
						$data[$c][5] = $year."-".$month."-".$day." 00:00:00";
					}else{
						$data[$c][5] = date("Y-m-d H:i:s");
					}
					
				}
				
				
				$c++;
			}
			print_r($data);
			$csv = array_to_scv($data, false);
			file_put_contents($csvLocation, $csv);
		}

	}
}			

/*
$dir = 'C:/ScrapeData/fatrustee';

$stateDir = scandir($dir);
if(!empty($stateDir)){
	

	foreach($stateDir as $state){
		if(isset($state) && !empty($state)){
			$files = glob($dir."/".$state."/*.csv");
			
			fixHeader($files);
		}
	}
}
*/

$basedir = 'C:/ScrapeDataHistory/';

$historyDir = scandir($basedir);
if(!empty($historyDir)){

	foreach($historyDir as $dir){
		
		if($dir != ".." && $dir != "."){
			$currentHistoryDIR = $dir;
			$FAdir =  $basedir.$dir."/fatrustee";
			if(is_dir($FAdir)){ 
				
				$stateDir = scandir($FAdir);
				if(!empty($stateDir)){
					
					foreach($stateDir as $state){
						
						if(isset($state) && !empty($state)){							
							$files = glob($FAdir."/".$state."/*.csv");
							fixheader($files, $currentHistoryDIR);
						}
					}//foreach
				}
			}
			
		}
	}//foreach end

}



?>