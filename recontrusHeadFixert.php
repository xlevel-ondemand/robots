<?php
include 'arrayToCsv.php';

class RecontrustParser 
{
	private $data;
	private $dir = 'C:/ScrapeDataHistory/02-05-2013/recontrust/';
    private $backup_dir = 'C:/ScrapeData/history/recontrust/';
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
	
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	public function parseRecontrust()
	{
        $this->stateDir = scandir($this->dir);
            if(!empty($this->stateDir)):
            /*
             * parse the csv data into sql data
             */
            foreach($this->stateDir as $state){
                if(isset($state) && !empty($state)){
                    $this->parseData($state);
				}
			}
        endif;
	}
	
	private function parseData($state)
	{
		$this->files = glob($this->dir.$state."/*.csv");
		echo "<pre>+".$state."</pre>";
		
		$header =  Array('Property Address', 'Opening Bid Amount', 'Place of Sale','Date of Sale', 'Make checks payable to');

		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$file."<br>";
				$data = $this->convertToArray($file);
				array_unshift($data,$header);
				
				$csv = array_to_scv($data, false);
				
				#print_r($csv);
				file_put_contents($file, $csv);
				
			endif;
		endforeach;
	}
	
	

}

$rparser = new RecontrustParser();
$rparser->parseRecontrust();

