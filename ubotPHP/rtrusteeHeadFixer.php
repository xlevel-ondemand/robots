
<?php
include 'arrayToCsv.php';

class RtrusteeHeadFixer 
{
	private $data;
	private $dir = 'C:/ScrapeData/rtrustee/';
    private $backup_dir = 'C:/ScrapeData/history/Rtrustee/';
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
	
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	public function parseRtrustee()
	{
        $this->stateDir = scandir($this->dir);
            if(!empty($this->stateDir)):
            /*
             * parse the csv data into sql data
             */
            foreach($this->stateDir as $state){
                if(isset($state) && !empty($state)){
                    $this->parseData($state);
				}
			}
        endif;
	}
	
	private function parseData($state)
	{
		$this->files = glob($this->dir.$state."/*.csv");
		echo "<pre>+".$state."</pre>";
		
		$header =  array(
					'no',
					'id',
					'Trustee Sale no',
					'',
					'Trustor',
					'Sale Time and date',
					'Sale Date',
					'Sale Time',
					'Sale Place',
					'Opening Bid Amount',
					'','','','',
					'File close',
					'Status',
					'',
					'State code',
					'','','','',
					'County',
					'Street',
					'City',
					'State code2',
					'Zip Code'
					);

		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$file."<br>";
				$data = $this->convertToArray($file);
				array_unshift($data,$header);
				#echo '<pre>';
				#print_r($data);
				
				$csv = array_to_scv($data, false);
				
				#print_r($csv);
				file_put_contents($file, $csv);
				
			endif;
		endforeach;
	}
	
	

}

$rparser = new RtrusteeHeadFixer();
$rparser->parseRtrustee();


?>